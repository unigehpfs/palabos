# From CPU to GPU in 80 days: description of the work achieved in the work packages

## WP 1: Implementation of test cases

Three Palabos applications were implemented which we plan to use to validate the GPU code developed within this project. All three can be run in regression mode, in which the codes report if the simulated results are consistent with those of previous simulation runs. The three test cases are:

### 1. The Taylor-Green vortex
An initial value problem, the decay of a system of vortices is simulated and followed through time. The simulation has no boundary condition (all boundaries are periodic), no obstacles, and the collision model and viscosity are homogeneous and time independent. Consequently, no data processors or chained dynamics objects need to be used during the execution of the simulation, although a data processor is required to set up the simulation. This is the simplest test case, which is expected to be ported to GPU first. The code is provided [here](https://gitlab.com/unigehpfs/palabos/-/tree/master/examples/80days/tgv3d).

### 2. Flow through a porous sandstone
A flow through a porous stone (a Berea sandstone) with resolution 400x400x400 is simulated at low Reynolds, with flow-flow conditions applied to an inlet and an outlet buffer zone. The simulation ultimately reaches a stationary state, at which the permeability of the sandstone is deduced from the pressure drop between inlet and outlet. We use a local boundary condition, and the simulation uses four types of collision models: bulk flow (TRT model), inlet (regularized BC), outlet (regularized BC), and solid (bounce-back). Still, no data processor is required during execution of the simulation (a data processor is required to set up the initial state, though). Compared to the Taylor-Green vortex, this example therefore allows to test if the concept of cell-dependent dynamics objects is properly ported to GPU. The code is provided [here](https://gitlab.com/unigehpfs/palabos/-/tree/master/examples/80days/sandstone).

### 3. Rayleigh-Taylor instability in a multi-component flow
Here, a gravitational interface instability in a multi-component flow is simulated with the help of a pseudo-potential approach ("the Shan/Chen multi-component model"). This is the most complex problem, as a data processor is used to couple the velocity and interaction force term between the two components. The code is provided [here](https://gitlab.com/unigehpfs/palabos/-/tree/master/examples/80days/rayleighTaylor3D).

## WP 2: Implementation of the AcceleratedLattice
### Summary
In this work package, we implemented the `AcceleratedLattice`, an alternative to the MultiBlockLattice which we want to ultimately run on GPU. For now, it runs only on CPU, though (or at least, we have not yet tried to compile it on GPU), but it allows to use a hybrid MPI - Multithreading execution mode, something which did not exist in Palabos so far. The two crucial ingredients of the `AcceleratedLattice` are:

   * The loops are parallelized with OpenMP and with a C++ parallel-algorithms construct (you can choose between OpenMP and parallel-algorithms with help of a `#define` at the beginning of your program), both of which in principle allow to compile the code for GPU. 
   * The data is arranged in memory through structure-of-array (soa) instead of Palabos' classic array-of-structure (aos), because soa [leads to substantially better performance on GPU](https://doi.org/10.1371/journal.pone.0250306).

The original intent was to implement the `AcceleratedLattice` with help of the single-population, thread safe AA pattern, which we however abandoned in favor of the double-population approach, which is less tricky and therefore better adapted to the proof-of-principle philosophy of the present project. 

### Presentation of the AcceleratedLattice
The [`AcceleratedLattice3D`](https://gitlab.com/unigehpfs/palabos/-/blob/master/src/multiBlock/acceleratedLattice3D.h) is essentially a copy of the [`MultiBlockLattice3D`](https://gitlab.com/unigehpfs/palabos/-/blob/master/src/multiBlock/multiBlockLattice3D.h), but it is implemented in terms of the new [`AtomicAcceleratedLattice3D`](https://gitlab.com/unigehpfs/palabos/-/blob/master/src/atomicBlock/atomicAcceleratedLattice3D.h) instead of the original [`BlockLattice3D`](https://gitlab.com/unigehpfs/palabos/-/blob/master/src/atomicBlock/blockLattice3D.h). All interesting code can be found in the implementation of the [`AtomicAcceleratedLattice3D`](https://gitlab.com/unigehpfs/palabos/-/blob/master/src/atomicBlock/atomicAcceleratedLattice3D.hh). The aos single-population data space of the `BlockLattice3D`, which is declared through a simple cell-array:

```
    Cell<T,Descriptor>     *rawData;
    Cell<T,Descriptor>   ***grid;
```

is replaced by a double population soa data space which separates the populations (direction after direction), external scalars, and dynamics objects:

```
    T                         *populations, *tmpPopulations;
    T                      ****populationGrid, ****tmpPopulationGrid;
    External                  *externalScalars;
    External                ***externalScalarGrid;
    Dynamics<T,Descriptor>*   *dynamicsArray;
    Dynamics<T,Descriptor>* ***dynamicsGrid;
```

During a collision-streaming cycle, the populations are read from the array `populations`, collided, and streamed into `tmpPopulations`. Once the cycle is over, `populations` and `tmpPopulations` are swapped.

Compared to the original implementation in the `Blocklattice3D`, this approach doubles the memory requirements and also leads, at least on CPU, to a somewhat slower code. It is a necessity, though, because the original swap-based implementation in Palabos is not thread safe, and could therefore not be parallelized with OpenMP or parallel algorithms. The AA-pattern, which we originally intended to use, would allow to achieve thread safety in a single-population implementation. But its implementation in Palabos turned out to be more tricky than expected, and we put it aside for now to allow a timely completion of the project, and to keep everything simpler. The double-population implementation also responds to a requirement of several people who were asking for the availability of such a data structure in Palabos to allow implementing complex algorithms, most notably for boundary conditions. Thanks to the double population implementation, data processors get access to populations of the current time step _and_ the populations of the previous time step.

The `AcceleratedLattice` uses OpenMP / parallel algorithms for multi-threaded parallelism, but we also want to keep MPI to achieve hybrid parallelism, and run the code on multiple nodes. MPI communication is obviously affected by the implementation choices of the `AcceleratedLattice`, because the data is no longer found at the same place in memory. Fortunately, not a single line of the MPI communication algorithms of Palabos needed to be adapted, thanks to the modular approach of Palabos. Every implementation of the atomic-block (`BlockLattice3D`, `ScalarField3D`, `AtomicAcceleratedLattice3D`, ...) also provides a so-called data-transfer class, which appropriately packs and unpacks the data for the MPI communication algorithm. A corresponding data-transfer class was written for the `AtomicAcceleratedLattice3D`, which makes sure that the data packing and unpacking is parallelized and can consequently be executed on the GPU.

Except for the `AcceleratedLattice3D` and `AtomicAcceleratedLattice3D` class (with corresponding `AcceleratedLatticeDataTransfer3D`), the only other extensions to Palabos that needed to be implemented are [some data processors](https://gitlab.com/unigehpfs/palabos/-/blob/master/src/dataProcessors/acceleratedProcessors3D.h) (because the original data processors no longer work for the AcceleratedLattice - but they are easy to port, as shown below). Except for this, Palabos remains unchanged (we replaced `MPI_Init` by `MPI_Init_thread`, though, to make sure the MPI implementation can run a hybrid simulation).

### Using the AcceleratedLattice
Using the `AcceleratedLattice` amounts essentially to simply replacing occurrences of `MultiBlockLattice3D` by `AcceleratedLattice3D` in end-user code. Before including the Palabos library, indicate if you prefer to use the paralell-algorithms or the OpenMP implementation:

```
#include "atomicBlock/acceleratedDefinitions.h"
#define ACCELERATOR PAR_ALG
//#define ACCELERATOR OMP_HOMOGENEOUS

#include "palabos3D.h"
#include "palabos3D.hh"
```

All dynamics objects work out of the box with the `AcceleratedLattice`, even complex, chained dynamics objects (we expect to run into issues on GPUs with this, though). Data processors on the other hand don't work, because they explicitly expect a aos data layout. It is easy to port data processors to the `AcceleratedLattice` (see below), but there exists another workaround that allows to run many existing Palabos applications without further adaptation. The idea is to run all pre- and post-processing with a `MultiBlockLattice` and the collision-streaming cycles with the `AcceleratedLattice`: 

```
    Dynamics<T,DESCRIPTOR> *dyn = new BGKdynamics<T,DESCRIPTOR>(omega);
    MultiBlockLattice3D<T, DESCRIPTOR> lattice (nx, ny, nz, dyn); // Set up a non-accelerated multi-block lattice
    simulationSetup(lattice); // Do all pre-processing on the multi-block lattice

    AcceleratedLattice3D<T, DESCRIPTOR> accLattice(lattice); // Copy all data to an accelerated lattice
    for (plint iT=0; iT<tmax; ++iT) {
        if (doOutput) {
            accLattice.writeBack(lattice); // Copy back to the multi-block lattice
            writeVTK(lattice); // Perform output from the multi-block lattice
        }
        accLattice.collideAndStream(); // Collision-streaming is done on the accelerated lattice
    }

```

If on the other hand you prefer to run the everything directly on the accelerated lattice, all data processors executed on the accelerated lattice must be adjusted, because the data (populations, external scalars, dynamics objects) must be gathered (PULL operation) before the action is executed, and written back to memory (PUSH operation) in case of modifications, after all the operations on a cell are complete. For example, the `computeVelocity` function calls a data processor which originally looks as follows:

```
template<typename T, template<typename U> class Descriptor> 
void BoxVelocityFunctional3D<T,Descriptor>::process (
        Box3D domain, BlockLattice3D<T,Descriptor>& lattice, TensorField3D<T,Descriptor<T>::d>& tensorField)
{
    Dot3D offset = computeRelativeDisplacement(lattice, tensorField);
    for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
        for (plint iY=domain.y0; iY<=domain.y1; ++iY) {
            for (plint iZ=domain.z0; iZ<=domain.z1; ++iZ) {
                lattice.get(iX,iY,iZ).computeVelocity (
                        tensorField.get(iX+offset.x,iY+offset.y,iZ+offset.z) );
            }
        }
    }
}
```

The version adjusted for accelerated lattices (already implemented for you) looks as follows:

```
template<typename T, template<typename U> class Descriptor> 
void AccelVelocityFunctional3D<T,Descriptor>::process (
        Box3D domain, AtomicAcceleratedLattice3D<T,Descriptor>& lattice, TensorField3D<T,Descriptor<T>::d>& tensorField)
{
    Dot3D offset = computeRelativeDisplacement(lattice, tensorField);
    for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
        for (plint iY=domain.y0; iY<=domain.y1; ++iY) {
            for (plint iZ=domain.z0; iZ<=domain.z1; ++iZ) {
                Cell<T,Descriptor> cell;
                lattice.pull(iX, iY, iZ, cell); // Gather all data into a cell
                cell.computeVelocity (
                        tensorField.get(iX+offset.x,iY+offset.y,iZ+offset.z) );
                // If the cell had been modified here (but it hasn't), you would need 
                // to write back the data:
                // lattice.push(iX, iY, iZ, cell);
            }
        }
    }
}
```

### Performance
For now, the codes are tested only on CPU, and different versions are available:

   * The original MPI-parallel code based on the single-population Swap algorithm
   * The new AcceleratedLattice code parallelized with MPI
   * The new AcceleratedLattice code parallelized with OpenMP or parallel-algorithms
   * The new AcceleratedLattice code with hybrid parallelism (MPI + OpenMP or parallel-algorithms)

The AcceleratedLattice code turns out to be slower than the original MultiBlockLattice on CPU, presumably because of the larger memory footprint. We however expect that on GPU, [the performance differences will be smaller](https://gitlab.com/unigehpfs/stlbm/). Here is the performance of the code for a flow through a porous sandstone, with different parallelization methods (performance is measured in MLUPS). The machine is a 48-core Intel Xeon Gold 6240R CPU @ 2.40GHz, and the code is executed with the command `mpirun -np num_tasks -bind-to socket -map-by socket ./sandstone`

| Implementation              | MPI Tasks | OpenMP threads per task | MLUPS  |
| --------------------------- | --------- | ----------------------- | ------ |
| MultiBlockLattice           | 48        |                         | 192.0  |
| AcceleratedLattice (OMP)    | 48        | 1                       | 116.0  |
| AcceleratedLattice (OMP)    | 1         | 48                      | 130.0  |
| AcceleratedLattice (OMP)    | 2         | 24                      | 136.8  |
| AcceleratedLattice (paralg) | 1         |                         | 142.3  |
